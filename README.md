# Sass for lib.rs

This is just the CSS for the lib.rs project.

## Usage

Requires Node.js and latest Rust. Install:

```sh
npm i
```

Assuming you have full checkout of the entire project (not just styles dir), you start the full crates server on localhost with:

```sh
( cd ../server; cargo run ) &
```

and then:

```sh
npm start
```

and then edit `*.scss` files in `src/`.

Open:

  * http://localhost:3000/
  * http://localhost:3000/no-std
  * http://localhost:3000/bitflags

When `gulp` is running, the CSS will update and reload automatically.

## License

This project: Apache/MIT.
The Fira font: SIL Open Font License.
