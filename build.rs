use grass_compiler as grass;
use lightningcss::stylesheet::{MinifyOptions, ParserOptions, PrinterOptions, StyleSheet};
use lightningcss::targets::{Browsers, Targets};
use std::path::Path;
use std::process::ExitCode;

fn main() -> ExitCode {
    if let Err(e) = run() {
        eprintln!("error: {e}");
        if let Some(e) = e.source() {
            eprintln!("{e}");
        }
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}
fn run() -> Result<(), Box<dyn std::error::Error>> {
    for entry in std::fs::read_dir("src")? {
        let entry = entry?;
        let path = entry.path();
        let file_name = path.file_name().unwrap().to_str().unwrap();
        if file_name.starts_with(['.', '_']) || !file_name.ends_with(".scss") {
            continue;
        }
        let dest = Path::new("public/").join(file_name).with_extension("css");
        println!("cargo:rerun-if-changed={}", path.display());
        println!("cargo:rerun-if-changed={}", dest.display());
        let css = grass::from_path(&path, &grass::Options::default())?;
        let mut style = StyleSheet::parse(&css, ParserOptions {
            filename: file_name.to_string(),
            error_recovery: false,
            ..Default::default()
        }).unwrap();
        let targets = Targets::from(Browsers {
            chrome: Some(100 << 16),
            firefox: Some(100 << 16),
            ie: None,
            ios_saf: Some(13 << 16),
            safari: Some(13 << 16),
            ..Default::default()
        });
        style.minify(MinifyOptions {
            targets,
            ..Default::default()
        }).unwrap();
        let css = style.to_css(PrinterOptions {
            targets,
            minify: true,
            project_root: Some("src"),
            ..Default::default()
        }).unwrap();
        if std::fs::read_to_string(&dest).is_ok_and(|old| old == css.code) {
            continue; // don't invalidate builds
        }
        std::fs::write(&dest, css.code)?;
    }
    Ok(())
}
